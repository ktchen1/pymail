from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
from email.mime.base import MIMEBase #用於承載附檔
from email import encoders #用於附檔編碼
import smtplib

class pymail:

    def __init__(self, sender, recipients, password, subject):
        self.sender = sender                #寄件者
        self.recipients = recipients        #收件者
        self.password = password            #密碼
        self.subject = subject              #郵件標題
        self.mail = MIMEMultipart()         #建立MIMEMultipart物件
        self.mail["subject"] = self.subject  
        self.mail["from"] = self.sender  
        self.mail["to"] = self.recipients 

    def Content(self,text=None,images=None,files=None):
        # text : 輸入字串
        # images : 輸入存有圖片檔名的list
        # files :  輸入存有檔案的list
        if text != None:
            self.mail.attach(MIMEText(text))
        
        if images != None:
            for img in images:
                with open(img,'rb') as fp:
                    msgImage = MIMEImage(fp.read(),_subtype=["png","jpg","jpeg"])

                msgImage.add_header('Content-ID', '<image1>')
                self.mail.attach(msgImage)

        if files != None:
            for file in files:
                with open(file, 'rb') as fp:
                    add_file = MIMEBase('application', "octet-stream")
                    add_file.set_payload(fp.read())
                encoders.encode_base64(add_file)
                add_file.add_header('Content-Disposition', 'attachment', filename=file)
                self.mail.attach(add_file)

    def Send(self):
        with smtplib.SMTP(host="smtp.gmail.com", port="587") as smtp:  # 設定SMTP伺服器
            try:
                smtp.ehlo()  # 驗證SMTP伺服器
                smtp.starttls()  # 建立加密傳輸
                smtp.login(self.sender, self.password)  # 登入寄件者gmail
                smtp.send_message(self.mail)  # 寄送郵件
                print("Complete!")
            except Exception as e:
                print("Error message: ", e)



